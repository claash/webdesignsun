var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	autoprefixerOptions = {
		overrideBrowserslist: [ 'last 2 versions' ]
	},
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	browserSync = require('browser-sync'),
	fileinclude = require('gulp-file-include');


// compile sass
gulp.task('sass', () => {
	return gulp
		.src('./assets/src/sass/app.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
		.pipe(gulp.dest('./assets/dist/css'))
		.pipe(autoprefixer(autoprefixerOptions))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('./assets/dist/css'))
		.pipe(browserSync.stream());
});

// Move the javascript files into our /src/js folder
gulp.src('./node_modules/jquery/dist/jquery.min.js').pipe(gulp.dest('./assets/dist/js/'));

// compile js
gulp.task('js', () => {
	return gulp
		.src('./assets/src/js/app.js')
		.pipe(
			fileinclude({
				prefix: '@@',
				basepath: '@file'
			})
		)
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./assets/dist/js'));
});


// Static Server + watching files
gulp.task(
	'serve',
	gulp.series('sass', function() {
		browserSync.init({
			proxy: "webdessun.local",
			notify: false,
			port: 8000
		});

		gulp.watch([ './assets/src/sass/*.scss', './assets/src/sass/**/*.scss' ], gulp.series('sass'));
		gulp.watch('./assets/src/js/*.js', gulp.series('js')).on('change', browserSync.reload);
		gulp.watch(['./*.php', './**/*.php']).on('change', browserSync.reload);
	})
);

gulp.task('default', gulp.parallel('sass', 'js', 'serve'));
