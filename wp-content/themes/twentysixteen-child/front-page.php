<?php 

get_header();

get_template_part('template-parts/template', 'hero');

$portfolio = get_posts(
		array(
			'numberposts' => 0,
			'post_type'	=> 'portfolio',
			'post_status' => 'publish'
		)
);

$content = apply_filters('the_content', $post->post_content);

?>

<section class="services">
	<div class="container">
		<div class="services-list">

			<?php foreach (get_field('services') as $service): ?>
				
				<div class="services-item">
					<figure>
						<img src="<?php echo $service['icon'] ?>" alt="">					
					</figure>
					<h2><?php echo $service['title']; ?></h2>
					<p><?php echo $service['text']; ?></p>
				</div>

			<?php endforeach; ?>
		</div>
	</div>
</section>

<?php if(!empty($portfolio)): ?>

<section class="portfolio">
	<div class="container">
		<h2>PORTFOLIO</h2>
		<div class="portfolio-slider">
			
			<?php foreach ($portfolio as $item): ?>

				<div class="portfolio-slider__item">
					<a href="<?php the_permalink($item); ?>" title="">
						<img src="<?php echo get_the_post_thumbnail_url($item->ID) ?>" alt="<?php echo $item->post_title; ?>">
						<h3><?php echo $item->post_title; ?></h3>
					</a>
				</div>

			<?php endforeach; ?>

		</div>
	</div>
</section>

<?php endif; ?>

<section class="home-content">
	<div class="container">
		<h2><?php the_title(); ?></h2>
		<main>
			<figure>
				<figcaption><?php the_post_thumbnail_caption(); ?></figcaption>
				<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
			</figure>
			<div class="home-content__text">
				<?php echo $content; ?>
			</div>
		</main>
	</div>
</section>

<?php

get_footer();