<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<footer class="footer">
	<div class="footer-main">
		<div class="container">
			<div class="footer-nav">
				<h3>NAVIGATION</h3>

				<?php 

					wp_nav_menu( array(
						'theme_location'  => 'footer',
						'menu'            => 'footer',
						'menu_class'	  => '',
						'container'       => '',
					) );

				?>
			</div>

			<div class="footer-contact">
				<h3>CONTACTS</h3>
				<ul>
					<?php if (!empty(get_theme_mod('footer_phone'))): ?>
					<li>
						<a href="tel:<?php echo str_replace(' ', '', get_theme_mod('footer_phone')) ; ?>" title="">
							<img src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/img/phone.png' ?>" alt="">
							<?php echo get_theme_mod('footer_phone'); ?>
						</a>
					</li>
					<?php endif; ?>

					<?php if (!empty(get_theme_mod('footer_email'))): ?>
					<li>
						<a href="mailto:<?php echo get_theme_mod('footer_email'); ?>" title="">
							<img src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/img/email.png' ?>" alt="">
							<?php echo get_theme_mod('footer_email'); ?>
						</a>
					</li>
					<?php endif; ?>

					<?php if (!empty(get_theme_mod('footer_location'))): ?>
					<li>
						<img src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/img/map.png' ?>" alt="">
						<?php echo get_theme_mod('footer_location'); ?>
					</li>
					<?php endif; ?>
				</ul>
			</div>

			<?php dynamic_sidebar('footer'); ?>
		</div>
	</div>
	<div class="footer-bt">
		<div class="container">
			© Copyright <?php echo date('Y') . ' | ' . get_theme_mod('footer_copyrights', 'copyrights'); ?>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
