if ($('.hero').length) {
    $('.hero').slick({
        dots: true,
        prevArrow: `<button type="button" class="slick-prev"></button>`,
        nextArrow: `<button type="button" class="slick-next"></button>`
    });
};

if ($('.portfolio-slider').length) {
    $('.portfolio-slider').slick({
        prevArrow: `<button type="button" class="slick-prev"></button>`,
        nextArrow: `<button type="button" class="slick-next"></button>`,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [{
            breakpoint: 576,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        }]
    });
};

// default validation fo cf7
$('.wpcf7-validates-as-required').prop('required', true);
$(".wpcf7-form").removeAttr('novalidate');

// prevent click for sub menus on portable divices
$('.menu-item-has-children').click(function(e) {

    if ($(window).width() < 992) {
        e.preventDefault();
    };

    $(window).resize(function() {
        if ($(window).width() < 992) {
            e.preventDefault();
        }
    });

});

$('.header button').click(function () {
    $(this).toggleClass('open');
    $('.header nav').toggleClass('open');

    if ($(this).hasClass('open')) {
        $('body').css('overflow', 'hidden');
    } else {
        $('body').css('overflow', 'auto');
    }
});