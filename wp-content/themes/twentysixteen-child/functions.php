<?php

// setup
function twentysixteen_setup() {
	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );

	add_theme_support(
			'html5',
			array(
				'gallery',
				'caption',
			)
		);

	add_theme_support(
		'custom-logo', array()
	);

	register_nav_menus(
		array(
			'primary' => __( 'Header Menu', 'twentysixteen-child' ),
			'footer'  => __( 'Footer Menu', 'twentysixteen-child' ),
		)
	);
}

add_action( 'after_setup_theme', 'twentysixteen_setup' );

// Hide items from admin panel
function admin_panel_hide () {
   remove_menu_page('edit.php');
   remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');
   remove_menu_page( 'edit-comments.php' );
};

add_action('admin_menu', 'admin_panel_hide');

// footer customizer
function twentysixteen_footer_customizer($wp_customize){

	$wp_customize->add_section('footer_settings_section', array(
		'title'          => 'Footer'
	));


	$wp_customize->add_setting('footer_phone', array(
	 	'default'        => '',
	));

	$wp_customize->add_setting('footer_email', array(
	 	'default'        => '',
	));

	$wp_customize->add_setting('footer_location', array(
	 	'default'        => '',
	));

	$wp_customize->add_setting('footer_copyrights', array(
	 	'default'        => '',
	));

	$wp_customize->add_control('footer_phone', array(
		'label'   => 'Footer phone',
		'section' => 'footer_settings_section',
		'type'    => 'text',
	));

	$wp_customize->add_control('footer_email', array(
		'label'   => 'Footer email',
		'section' => 'footer_settings_section',
		'type'    => 'email',
	));

	$wp_customize->add_control('footer_location', array(
		'label'   => 'Footer location',
		'section' => 'footer_settings_section',
		'type'    => 'text',
	));

	$wp_customize->add_control('footer_copyrights', array(
		'label'   => 'Footer copyrights',
		'section' => 'footer_settings_section',
		'type'    => 'text',
	));
};

add_action('customize_register', 'twentysixteen_footer_customizer');

// include styles and js
function twentysixteen_child_scripts () {

	wp_deregister_script('jquery');

	wp_enqueue_style( 'child-css', get_stylesheet_directory_uri() . '/assets/dist/css/app.css' );

	wp_enqueue_script( 'jquery', get_stylesheet_directory_uri() . '/assets/dist/js/jquery.min.js');

	wp_enqueue_script( 'child-js', get_stylesheet_directory_uri() . '/assets/dist/js/app.js', array('jquery'), '20190612', true );

}

add_action( 'wp_enqueue_scripts', 'twentysixteen_child_scripts' );

// CF7 remove wrappers and <br>
add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    return $content;
});

// widgets
function twentysixteen_child_widget_init() {

	register_sidebar( array(
		'name'          => 'Footer',
		'id' 			=> 'footer',
		'before_widget' => '<div class="footer-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'twentysixteen_child_widget_init' );

// Custom post type hero slider
function cpt_slider() {
 
    register_post_type( 'slider',
        array(
            'labels' => array(
                'name' => __( 'Hero slider' ),
                'singular_name' => __( 'Slide' )
            ),
            'description' => 'This is the top page slider in home page',
            'menu_icon' => 'dashicons-images-alt2',
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'slider'),
            'supports' => array( 'title', 'thumbnail', 'editor')
        )
    );
}

add_action( 'init', 'cpt_slider' ); 

// Custom post type portfolio
function cpt_portfolio() {
 
    register_post_type( 'portfolio',
        array(
            'labels' => array(
                'name' => __( 'Portfolio' ),
                'singular_name' => __( 'Item' )
            ),
            'menu_icon' => 'dashicons-portfolio',
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'portfolio'),
            'supports' => array( 'title', 'thumbnail' )
        )
    );
}

add_action( 'init', 'cpt_portfolio' ); 
