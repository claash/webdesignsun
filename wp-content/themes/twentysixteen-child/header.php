<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

$custom_logo_id = get_theme_mod( 'custom_logo' );
$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header class="header">
		<div class="container">
			<a class="header-logo" href="<?php echo site_url(); ?>">
				<img src="<?php echo $logo[0]; ?>" alt="<?php bloginfo('name') ?>">
			</a>
			<nav>
				<?php 

				wp_nav_menu( array(
					'theme_location'  => 'header',
					'menu'            => 'header',
					'container'       => '',
				) );

				?>
			</nav>
			<button>
				<span></span>		
				<span></span>		
				<span></span>		
			</button>
		</div>
	</header>