<?php 

$slides = get_posts(
		array(
			'numberposts' => 0,
			'post_type'	=> 'slider',
			'post_status' => 'publish'
		)
);

if (!empty($slides)): ?>

<section class="hero">

	<?php foreach ($slides as $slide): ?>

		<div class="hero-slide">
			<img src="<?php echo get_the_post_thumbnail_url($slide->ID) ?>" alt="<?php echo $slide->post_title; ?>">
			<div class="container">
				<div class="hero-slide__text">
					<h1><?php echo $slide->post_title; ?></h1>
					<p><?php echo $slide->post_content; ?></p>
				</div>
			</div>
		</div>

	<?php endforeach; ?>

</section>

<?php endif; ?>